#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <set>
#include <map>
#include <list>
using namespace std;


using Point = std::pair<int,int>;
using TMap = vector<string>;
using TSolutions = vector<TMap>;

enum class Direction {U,D,L,R};
map<Direction, char> replChar = {{Direction::U,'^'},{Direction::D,'v'},{Direction::L,'<'},{Direction::R,'>'}};
set<char> Obstacles ={'X','<','^','>','v'};

class Ball;
ostream& operator<<(ostream& os, const Ball& b);
ostream& operator<<(ostream& os, const vector<string>& v);

TMap operator-(TMap m1, TMap m2)
{
    TMap retMap= m1;
    for(int i =0; i < m1.size(); i++)
        for(int j=0; j<m1[0].length(); j++)
            if(m1[i][j]==m2[i][j])
                retMap[i][j] = '.';
    return retMap;
}

TMap& PrepareSolutionMap(TMap& myMap)
{
    for(int i=0; i < myMap.size(); i++)
        for(int j=0; j<myMap[i].length(); j++)
            if(myMap[i][j]!='^' && myMap[i][j]!='v' && myMap[i][j]!='<' && myMap[i][j]!='>' && myMap[i][j]!='o')
                myMap[i][j] ='.';
    return myMap;
}

TMap& PrepareResultMap(TMap& myMap)
{
    for(int i=0; i < myMap.size(); i++)
        for(int j=0; j<myMap[i].length(); j++)
            if(myMap[i][j]!='^' && myMap[i][j]!='v' && myMap[i][j]!='<' && myMap[i][j]!='>')
                myMap[i][j] ='.';
    return myMap;
}

class Ball
{
public:
    Ball(char sc, Point pos): shot_cnt(sc-'0'), startPos(pos)
    {
        position = startPos;
        ballCounter++;
        id = ballCounter;
        isAtStart = true;
    }
    void Solve();
    void setMap(TMap _map) {currMap = _map; startMap = _map;}
    bool operator==(char c);
    char& operator=(char c);
    static int ballCounter;
    bool isAtStart;
    int validSolutionIdx;
    int id;
    int shot_cnt;
    TMap currMap;
    TMap startMap;
    Point startPos;
    Point position;
    TSolutions solutions;
    bool move(Direction d);
    friend ostream& operator<<(ostream& os, const Ball& b);
};

bool Ball::operator==(char c)
{
        if (this->currMap[position.first][position.second] == c)
            return true;
        return false;
}
char& Ball::operator=(char c)
{
    this->currMap[position.first][position.second] = c;
    return c;
}
bool Ball::move(Direction d)
{
    Ball backup = *this;
    TMap tmpMap = currMap;
    Point nextP;
    int tmpStartIdx, tmpStopIdx;
    char axis;
    int it;
    switch (d)
    {
        case Direction::U:
            nextP = std::make_pair(position.first-shot_cnt, position.second);
            it = -1;
            axis = 'V';
            break;
        case Direction::D:
            nextP = std::make_pair(position.first+shot_cnt, position.second);
            axis = 'V';
            it = 1;
            break;
        case Direction::L:
            nextP = std::make_pair(position.first, position.second-shot_cnt);
            it = -1;
            axis = 'H';
            break;
        case Direction::R:
            nextP = std::make_pair(position.first, position.second+shot_cnt);
            it =1;
            axis = 'H';
            break;
    }
    if(nextP.first<0 ||nextP.first>=currMap.size() || nextP.second<0 || nextP.second>=currMap[0].length())
    {
        //cerr << "false1" << endl;
        return false;
    }
    if(Obstacles.find(currMap[nextP.first][nextP.second])!=Obstacles.end())
    {
        //cerr <<"false2" << endl;
        return false;
    }
    if(axis == 'H')
    {
        for(int i = position.second; i!= nextP.second; i+=it)
        {
            char tmpCh = tmpMap[position.first][i];
            // if(tmpCh == 'H')
            // {
            //     //cerr << "&^&^&^&^&^&^&^&^&^&^&^^&^&^&^&&^^&\n";
            //     nextP.first = position.first;
            //     break;
            // }
            if (tmpCh!='.' && tmpCh!='X' && tmpCh!='H' && !this->isAtStart)
            { 
                return false;
            }
            tmpMap[position.first][i]=replChar[d];
        }
    }
    else
    {
        for(int i = position.first; i!= nextP.first; i+=it)
        {
            char tmpCh = tmpMap[i][position.second];
            // if(tmpCh == 'H')
            // {
            //     //cerr << "&^&^&^&^&^&^&^&^&^&^&^^&^&^&^&&^^&\n";
            //     nextP.second = position.second;
            //     break;
            // }
            if (tmpCh!='.' && tmpCh!='X' && tmpCh!='H'&& !this->isAtStart)
            {
                //cerr << "false4"<<endl;
                return false;
            }
            tmpMap[i][position.second]=replChar[d];
        }
    }
    if (isAtStart) isAtStart=false;
    currMap = tmpMap;
    position = nextP;
    shot_cnt--;
    return true;
}

int Ball::ballCounter=0;


ostream& operator<<(ostream& os, const Ball& b)
{
    os << "ball no:" << b.id <<" sc:"<<b.shot_cnt<< " pos:" << b.position.first << "," << b.position.second;
    return os;
}

ostream& operator<<(ostream& os, const vector<string>& v)
{
    for (auto s: v)
        os << s << endl;
    return os;
}

void Ball::Solve(/*Ball& b*/)
{
    if (*this=='H' /*&& b.shot_cnt == 0*/) // pytanie czy musi to byc ostatnie uderzenie, ni chuja nie jestem pewien
    {
        PrepareSolutionMap(this->currMap);
        (*this)='o'; //o means that there is a ball in this hole
        this->solutions.push_back(this->currMap - this->startMap);
        this->startMap.clear();
        return;
    }
    if(this->shot_cnt == 0)
        return;
    Ball tmpBall1 =(*this);
    Ball tmpBall2 =(*this);
    Ball tmpBall3 =(*this);
    Ball tmpBall4 =(*this);
    if (tmpBall1.move(Direction::U))
    {
        tmpBall1.Solve();
        this->solutions.insert(this->solutions.end(), tmpBall1.solutions.begin(), tmpBall1.solutions.end());
    }
    if (tmpBall2.move(Direction::D))
     {
         tmpBall2.Solve();
        this->solutions.insert(this->solutions.end(), tmpBall2.solutions.begin(), tmpBall2.solutions.end());
     }
    if (tmpBall3.move(Direction::L))
     {
         tmpBall3.Solve();
         this->solutions.insert(this->solutions.end(), tmpBall3.solutions.begin(), tmpBall3.solutions.end());
     }
    if (tmpBall4.move(Direction::R))
     {
         tmpBall4.Solve();
         this->solutions.insert(this->solutions.end(), tmpBall4.solutions.begin(), tmpBall4.solutions.end());
     }

}

void AddSolutionToMap(TMap &mainMap, const TMap& solution)
{
    //cerr << "dodaje solution do mapy:" << endl;
    for(int i = 0; i < solution.size(); i++)
        for (int j=0; j < solution[0].length(); j++)
            if(solution[i][j]!='.')
                mainMap[i][j] = solution[i][j];
}

bool SolutionsOverlap(const TMap& m1, const TMap& m2)
{
    for(int i=0; i < m1.size(); i++)
        for(int j =0; j < m1.front().length(); j++)
            if (m1[i][j]!='.' && m2[i][j]!= '.')
            {
                return true;
            }
    return false;
}

bool IsItGoodPath(vector<Ball>& balls, int ballIdx, int solIdx, TMap &resMap)
{
    if(ballIdx==balls.size()-1)
    {
        AddSolutionToMap(resMap, balls[ballIdx].solutions[solIdx]);
        balls[ballIdx].validSolutionIdx = solIdx;
        return true;
    }
    int nextBIdx = ballIdx+1;
    TMap tmpMap = resMap;
    AddSolutionToMap(tmpMap, balls[ballIdx].solutions[solIdx]);
    for(int nextBSolIdx = 0; nextBSolIdx < balls[nextBIdx].solutions.size() ; nextBSolIdx++)
    {
       if(!SolutionsOverlap(tmpMap, balls[nextBIdx].solutions[nextBSolIdx])
            && IsItGoodPath(balls, nextBIdx, nextBSolIdx, tmpMap))
        {
             balls[ballIdx].validSolutionIdx = solIdx;
             resMap=tmpMap;
             return true;
        }
    }
    return false;
}
TMap MergeSolutions(vector<Ball>& balls, TMap resMap)
{
    for(int i =0; i < resMap.size(); i++)
        for (int j=0; j<resMap.front().length();j++)
            resMap[i][j]='.';
    //Sorting vector to not need to go back to far if the current analyzed solution is wrong
    std::sort(balls.begin(),balls.end(),[](const Ball& b1, const Ball& b2)->bool{ return b1.solutions.size() < b2.solutions.size();} );
    for (int k =0; k < balls[0].solutions.size(); k++)
        if (IsItGoodPath(balls, 0, k, resMap))
            return resMap;

    for(int i =0; i < resMap.size(); i++)
        for (int j=0; j<resMap.front().length();j++)
            resMap[i][j]='*';
    return resMap;
}
int main()
{
    int width;
    int height;
    TMap myMap;
    vector<Ball> balls;
    cin >> width >> height; cin.ignore();
    for (int i = 0; i < height; i++)
    {
        string row;
        cin >> row; cin.ignore();
        myMap.push_back(row);
        for(int j=0; j<width; j++)
        {
            if(isdigit(row[j]))
            {
                Ball b{row[j], std::make_pair(i,j)};
                balls.push_back(b);
            }
        }
    }
    cerr << myMap << endl;
    for(auto& b: balls)
    {
       b.setMap(myMap);
       b.Solve();
       //cerr << "ball no " << b.id << " no of solutions: " << b.solutions.size() << endl;
       if(b.solutions.size() == 1) //if there is only one solution it can be already added to main map
           AddSolutionToMap(myMap, b.solutions[0]);
    }
    myMap = MergeSolutions(balls, myMap);
    // for (auto b : balls)
    // {
    //     static int i =0;
    //     cerr << "SOlutions for ball :" << i << endl;
    //     for (auto s: balls[i].solutions)
    //         cerr << s << endl<<endl;
    //     i++;
    // }
    cerr << "\nRESULT:" << endl;
    cout << PrepareResultMap(myMap) << endl;
}
