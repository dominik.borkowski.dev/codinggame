#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
 class Node {
   public:
    int id;
    vector <int> sasiedzi;
    int gate;
     
    Node(int ajdi);    
    void addSas(int sas);
    void findGate(vector <int> gats);
    void sprawdz();
 };
 Node::Node(int ajdi){
     id = ajdi;
     gate=-1; 
     
 }
 void Node::sprawdz(){
     cerr << "sprawdzam" << endl;
     cerr<<id<< " "<< gate << endl; 
    if (gate!=-1) {
      cout<<id<< " "<<gate << endl;  
    }
    else
      cout<<id<<" "<<sasiedzi[0]<<endl;    
    
 }
 void Node::addSas(int sas) {
    for(int i=0; i< sasiedzi.size(); i++){
        if (sas == sasiedzi[i])
            return;
    }
        sasiedzi.push_back(sas); 
 } 
void Node::findGate(vector <int> gats){
    for( int i =0; i<gats.size(); i++){
        for (int j=0; j<sasiedzi.size(); j++){
            if(sasiedzi[j]==gats[i]){
                gate=gats[i];
                return;
            }
        }
    }   
};
int main()
{
    int N; // the total number of nodes in the level, including the gateways
    int L; // the number of links
    int E; // the number of exit gateways
    vector <Node> nodes;
    vector <int> gates;
    cin >> N >> L >> E; cin.ignore();
    
    for(int i=0; i< N; i++){
        Node tmp = Node(i);  
        nodes.push_back(tmp);
    }
    for (int i = 0; i < L; i++) {
        int N1; // N1 and N2 defines a link between these nodes
        int N2;
        cin >> N1 >> N2; cin.ignore();
        nodes[N1].addSas(N2);
        nodes[N2].addSas(N1);
    }
    for (int i = 0; i < E; i++) {
        int EI; // the index of a gateway node
        cin >> EI; cin.ignore();
        gates.push_back(EI);
        cerr << "dodaje gate:" << EI <<endl;
    }
    for (int i=0; i<nodes.size(); i++){
         nodes[i].findGate(gates);
    }
    // game loop
    while (1) {
        int SI; // The index of the node on which the Skynet agent is positioned this turn
        cin >> SI; cin.ignore();
        cerr<< "agent: " << SI << endl;
        nodes[SI].sprawdz();
        // Write an action using cout. DON'T FORGET THE "<< endl"
        // To debug: cerr << "Debug messages..." << endl;


        // Example: 0 1 are the indices of the nodes you wish to sever the link between
    }
}