#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include <sstream>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

 
int main()
{
    int W; // number of columns.
    int H; // number of rows.
    cin >> W >> H; cin.ignore();
    cerr << "w :" << W << " h :"<<H<<endl;
    
    map <int, map<string,char>> mapka;
    {
    mapka[0]["TOP"]='e';
    mapka[0]["LEFT"]='e';
    mapka[0]["RIGHT"]='e';
    
    mapka[1]["TOP"]='d';
    mapka[1]["LEFT"]='d';
    mapka[1]["RIGHT"]='d';
    
    mapka[2]["TOP"]='e';
    mapka[2]["LEFT"]='p';
    mapka[2]["RIGHT"]='l';
    
    mapka[3]["TOP"]='d';
    mapka[3]["LEFT"]='e';
    mapka[3]["RIGHT"]='e';
    
    mapka[4]["TOP"]='l';
    mapka[4]["LEFT"]='e';
    mapka[4]["RIGHT"]='d';
    
    mapka[5]["TOP"]='p';
    mapka[5]["LEFT"]='d';
    mapka[5]["RIGHT"]='e';
    
    mapka[6]["TOP"]='e';
    mapka[6]["LEFT"]='p';
    mapka[6]["RIGHT"]='l';
    
    mapka[7]["TOP"]='d';
    mapka[7]["LEFT"]='e';
    mapka[7]["RIGHT"]='d';
    
    mapka[8]["TOP"]='e';
    mapka[8]["LEFT"]='d';
    mapka[8]["RIGHT"]='d';
    
    mapka[9]["TOP"]='d';
    mapka[9]["LEFT"]='d';
    mapka[9]["RIGHT"]='e';
    
    mapka[10]["TOP"]='l';
    mapka[10]["LEFT"]='e';
    mapka[10]["RIGHT"]='e';
    
    mapka[11]["TOP"]='p';
    mapka[11]["LEFT"]='e';
    mapka[11]["RIGHT"]='e';
    
    mapka[12]["TOP"]='e';
    mapka[12]["LEFT"]='e';
    mapka[12]["RIGHT"]='d';
    
    mapka[13]["TOP"]='e';
    mapka[13]["LEFT"]='d';
    mapka[13]["RIGHT"]='e';
    }
    int** poleGry=new int * [W];
    for (int i = 0; i<W; i++)
        poleGry[i] = new int[H];
    
    for (int i = 0; i < H; i++) {
        string LINE; // represents a line in the grid and contains W integers. Each integer represents one room of a given type.
        string word;
        getline(cin, LINE);
        cerr<<"Line:"<<LINE<<endl;
        istringstream iss(LINE);
        for(int j =0; j< W; j++)
        {
          getline( iss, word, ' ');
          poleGry[j][i]=stoi(word);
        }
    }
            
    int EX; // the coordinate along the X axis of the exit (not useful for this first mission, but must be read).
    cin >> EX; cin.ignore();
    // game loop
    while (1) {
        int XI;
        int YI;
        string POS;
        cin >> XI >> YI >> POS; cin.ignore();
        cerr << "XI: " << XI <<" Yi: "<<YI<<" POS: "<<POS<<endl;
        switch (mapka[poleGry[XI][YI]][POS])
        {
            case 'l':
            cout << XI-1 << " " << YI << endl;
             break;
            case 'd':
            cout << XI << " " << YI +1 << endl;
             break;
            case 'p':
            cout << XI +1 << " " << YI << endl;
             break;
            case 'e':
             cerr << "Jestes w stanie Error!! " << endl;
             break;
            default:
            cerr << "Nie powinno tu Cie byc cos poszlo nie tak!!!! " << endl;
        }
        
        // Write an action using cout. DON'T FORGET THE "<< endl"
        // To debug: cerr << "Debug messages..." << endl;


        // One line containing the X Y coordinates of the room in which you believe Indy will be on the next turn.
       //cout << "0 0" << endl;
    }
    for (int i = 0; i<W; i++)
        delete [] poleGry[i];
    delete [] poleGry; 

}