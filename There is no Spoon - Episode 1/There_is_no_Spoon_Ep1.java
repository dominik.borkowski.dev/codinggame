import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Don't let the machines win. You are humanity's last hope...
 **/
class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int width = in.nextInt(); // the number of cells on the X axis - liczba column
        int height = in.nextInt(); // the number of cells on the Y axis - liczba wierszy
        //System.err.println("width: " + width + " height :" + height);        
        
        Board board = new Board(height, width);
        
        if (in.hasNextLine()) {
            in.nextLine();
        }
        for (int i = 0; i < height; i++) {
            String line = in.nextLine(); // width characters, each either 0 or .
            //System.err.println(line);
            board.addRow(line);
        }
         //System.err.print(board);
        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");


        // Three coordinates: a node, its right neighbor, its bottom neighbor
        System.out.println(board.getSolution());
    }
}
class Board {        
    private char[][] board;
    static private int rowNo =0;
    private int height;
    private int width;
    public Board(int height, int width) {
        board = new char[width][height];
        this.height=height;
        this.width = width;
    }
    public void addRow(String row) {
        if(rowNo>=height) {
            System.err.println("Error: you are trying to add to much rows");
        }
        for(int i=0; i < width; i ++)
            board[i][rowNo] = row.charAt(i);
        rowNo++;        
    }
    private String findRightNeighbour(int rzad, int col) {
        for(int j=col+1; j<width; j++){
            if(board[j][rzad]=='0')
                return j + " " + rzad + " ";
        }
        return "-1 -1 ";
    }
    private String findBottomNeighbour(int rzad, int col) {
        for(int i= rzad+1; i<height; i++) {
            if(board[col][i]=='0')
                return col+" "+i+"\n";
        }
        return "-1 -1\n";
    }
    public String getSolution() {
        String solution = "";
        for(int i=0; i<height; i++) {
            for(int j =0; j<width; j++){
                if(board[j][i] != '0')
                    continue;
                String nodeAnsw = j+" "+i+" ";
                nodeAnsw += findRightNeighbour(i, j);
                nodeAnsw += findBottomNeighbour(i, j);
                solution+=nodeAnsw;
            }
        }
        return solution;
    }
    @Override
    public String toString(){
        String result ="";
        for(int i=0; i<height; i++) {
            for(int j =0; j<width; j++)
                result += board[j][i];
            result += "\n";
        }
        return result;
    }
}