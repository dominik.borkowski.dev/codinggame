#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <memory>

using namespace std;

class AdjancencyMap
{
private:
	class Person
	{
	public:
		int id;
		int propTime;
		std::set<int> neighbours;
		std::map<int, int> ngbLongestWay;
		Person(int);
		bool isEdgeElement(); // ok
		int howFarCanGoThatWay(int, AdjancencyMap*); //ok
		void addNeighbour(int); //ok
		std::map<int, int>::iterator getMaxLongestWay(); //ok
	};
	std::map<int, std::shared_ptr<Person>> people;
public:
	int getSolution();
	AdjancencyMap();   //ok
	~AdjancencyMap(); //ok
	void addAdjancency(int, int); // ok
	void inspectInfo(); //ok
};
int AdjancencyMap::getSolution()
{
	auto comparator = [](const std::pair<int, int>& p1, const std::pair<int, int>& p2) {return p1.second < p2.second; };
	//1. determine propTime for first element
	//2. for neighbour with longest path compare if he have shorter longest path (dla sasiada z najdluzsza sciezka porownaj czy on ma krotsza najdluzsza sciezke)
	//3. if not return currNode->propTime ( jezeli nie to zwroc currNode->propTime)
	//4. if yes, move currentNode to NextNode and calculate howFarCanGoThatWay for previous point
	// (jezeli tak to zmien currentNode na next node i policz howFarCanGoThatWay dla punktu z ktorego przyszlismy)
	//go to 2
	auto currNode = people[1];
	for (auto nb : currNode->neighbours)
	{
		currNode->ngbLongestWay[nb] = people[nb]->howFarCanGoThatWay(currNode->id, this);
	}
	auto maxElement = std::max_element(currNode->ngbLongestWay.begin(), currNode->ngbLongestWay.end(), comparator);
	currNode->propTime = maxElement->second;
	
	auto nextNode = people[maxElement->first];
	//cerr << "currNode: " << currNode->id << "prop: " << currNode->propTime <<  " nextNode: " << nextNode->id << endl;

	//cerr << "nextNode max: " << nextNode->getMaxLongestWay()->second << endl;
	while (currNode->propTime > nextNode->getMaxLongestWay()->second)
	{
		nextNode->ngbLongestWay[currNode->id] = currNode->howFarCanGoThatWay(nextNode->id, this);
		currNode = nextNode;
		maxElement = currNode->getMaxLongestWay();
		currNode->propTime = maxElement->second;
		nextNode = people[maxElement->first];
		//cerr << "currNode: " << currNode->id << "prop: " << currNode->propTime << " nextNode: " << nextNode->id << endl;
		//cerr << "warunek: " << currNode->propTime << " > " << nextNode->getMaxLongestWay()->second << endl;
	}
	return currNode->propTime;
}

AdjancencyMap::AdjancencyMap(){}


AdjancencyMap::~AdjancencyMap(){}

void AdjancencyMap::addAdjancency(int p1, int p2)
{
	if (!people.count(p1))
	{
		people[p1] = std::make_shared<Person>(p1);
	}
	if (!people.count(p2))
	{
		people[p2] = std::make_shared<Person>(p2);
	}
	people[p1]->addNeighbour(p2);
	people[p2]->addNeighbour(p1);
}

void AdjancencyMap::inspectInfo()
{
	for (auto & p : people)
	{
		cerr << p.second->id << " ngbs: ";
		for (auto& i : p.second->neighbours)
			std::cerr << i << " ";
		cerr << std::endl;
	}
}

AdjancencyMap::Person::Person(int ajdi) : id(ajdi) {}

bool AdjancencyMap::Person::isEdgeElement()
{
	return this->neighbours.size()==1;
}

int AdjancencyMap::Person::howFarCanGoThatWay(int whoAsks, AdjancencyMap* owner)
{
	int retVal = -1;
	if (this->isEdgeElement())
		return 1;
	for (auto& i : neighbours)
	{
		if (i == whoAsks)
			continue;
		if(!ngbLongestWay.count(i))
		{
			ngbLongestWay[i] = owner->people[i]->howFarCanGoThatWay(this->id, owner);
		}
		if (retVal < ngbLongestWay[i])
			retVal = ngbLongestWay[i];
	}
	return retVal+1;
}


void AdjancencyMap::Person::addNeighbour(int i)
{
	neighbours.insert(i);
}

std::map<int,int>::iterator AdjancencyMap::Person::getMaxLongestWay()
{
	return std::max_element(
		ngbLongestWay.begin(),
		ngbLongestWay.end(),
		[](const std::pair<int, int>& p1, const std::pair<int, int>& p2) {return p1.second < p2.second; }
	);
}
int main()
{
    int n; // the number of adjacency relations    
    cin >> n; cin.ignore();
    AdjancencyMap mapka;
    for (int i = 0; i < n; i++) {
        int xi; // the ID of a person which is adjacent to yi
        int yi; // the ID of a person which is adjacent to xi
        cin >> xi >> yi; cin.ignore();
        mapka.addAdjancency(xi, yi);
    }
    cout << mapka.getSolution() << endl;
}