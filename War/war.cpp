#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
char bitwa(string g1, string g2, vector<string> karty)
{
    int i1=-1;
    int i2=-1;
    //cerr << "g1: " << g1 << endl;
    g1 = g1.substr(0,g1.size()-1); // usuniecie Suit
    g2 = g2.substr(0,g2.size()-1);
    
    for(int i=0; i< karty.size(); i++){
        if(g1==karty[i])
            i1=i;
        if(g2==karty[i])
            i2=i;
        if(i1!=-1 && i2!=-1)
            break;    
    }
    if(i1==i2)
        return 'r';
    else if (i1>i2)
        return 'p';
    else
        return 'd';
}
bool Wojna(vector<string>& vg1, vector<string>& vg2, vector<string> Tal)
{
    bool warEnd=false;
    char winner;
    int a=4;
    while(!warEnd){
        
        cerr << " w wojnie bitwa pomiedzy: " << vg1[a] << " vs "<< vg2[a]<<endl;
       if(a>=vg1.size() || a>=vg2.size())
            return true;
          cerr << "tu sie wywala na tescie 4 " << "a:"<< a << " "<<(a>=vg1.size()-1 || a>=vg2.size()-1);
        winner=bitwa(vg1[a], vg2[a], Tal);
        if(winner=='r' && (a>=vg1.size()-1 || a>=vg2.size()-1)){
            return true;
        }
        if(winner!='r') {
            warEnd=true;
            break;
        }
        a+=4;
    }
      
        if (winner=='p'){
            for(int b=0; b<=a; b++){
                vg1.push_back(vg1[0]);
                vg1.erase(vg1.begin()); 
            }
            for(int b=0; b<=a; b++){
                vg1.push_back(vg2[0]);
                vg2.erase(vg2.begin());  
            }
        }
        if (winner=='d'){
            for(int b=0; b<=a; b++){
                vg2.push_back(vg1[0]);
                vg1.erase(vg1.begin()); 
            }
            for(int b=0; b<=a; b++){
                vg2.push_back(vg2[0]);
                vg2.erase(vg2.begin());
            }
        }
    return false; 
}
vector<string> GenerujTalie()
{
    vector<string> kartyv;  
    kartyv.push_back("2");
    kartyv.push_back("3");
    kartyv.push_back("4");
    kartyv.push_back("5");
    kartyv.push_back("6");
    kartyv.push_back("7");
    kartyv.push_back("8");
    kartyv.push_back("9");
    kartyv.push_back("10");
    kartyv.push_back("J");
    kartyv.push_back("Q");
    kartyv.push_back("K");
    kartyv.push_back("A");
    return kartyv;
}
int main()
{
    int n; // the number of cards for player 1
    bool remis=false;
    int nrGames=0;
    vector <string> gracz1;
    vector <string> gracz2;    
    vector <string> Talia=GenerujTalie(); //generowanie tali kart
  //  string karty[13] ={"2","3","4","5","6","7","8","9","10","J","Q","K","A"};

    cin >> n; cin.ignore();
    for (int i = 0; i < n; i++) {
        string cardp1; // the n cards of player 1
        cin >> cardp1; cin.ignore();
        gracz1.push_back(cardp1);
         cerr << cardp1 << endl;
    }
    cerr <<endl;
    int m; // the number of cards for player 2
    cin >> m; cin.ignore();
    for (int i = 0; i < m; i++) {
        string cardp2; // the m cards of player 2
        cin >> cardp2; cin.ignore();
        gracz2.push_back(cardp2);
        cerr << cardp2 << endl;
    }
    
    while(!(gracz1.empty() || gracz2.empty()) && !remis  ){
       // cerr << "rozmiar g1 i g2 " << gracz1.size() << " : " << gracz2.size() << endl;
        static int ix1=0;
        static int ix2=0;
        if(ix1==gracz1.size())
            ix1=0;
        if(ix2==gracz2.size())
            ix2=0;
           // cerr << bitwa(gracz1[ix1], gracz2[ix2], Talia) << endl;
           cerr <<endl << gracz1[ix1] << " vs " << gracz2[ix2] <<endl;
           
        switch (bitwa(gracz1[ix1], gracz2[ix2], Talia)){
            case 'p':
                gracz1.push_back(gracz1[ix1]);
                gracz1.push_back(gracz2[ix2]);
                gracz1.erase(gracz1.begin()+ix1);
                gracz2.erase(gracz2.begin()+ix2);
                nrGames++;
                //ix1++;
                break;
            case 'd':                
                gracz2.push_back(gracz1[ix1]);
                gracz2.push_back(gracz2[ix2]);
                gracz2.erase(gracz2.begin()+ix2);
                gracz1.erase(gracz1.begin()+ix1);
                //ix2++;               
                nrGames++;
                break;
            case 'r':
                //cerr << "rozmiar g1 i g2 " << gracz1.size() << " : " << gracz2.size() << endl;
                if (ix1==gracz1.size()-1 || ix2==gracz2.size()-1)
                    remis = true;
                else
                   // remis = Wojna(gracz1, gracz2, ix1, ix2, Talia);
                    remis = Wojna(gracz1, gracz2, Talia);
                    nrGames++;
                  //  cerr << " po tej wojnie index 1:2 " << ix1 << "  " << ix2 << endl;
                break;
            default:
              cerr << "blad nie powinno cie tu byc" << endl;        
        }   
        //cerr << "stan kart:" << endl;
        for(int i=0; i<gracz1.size(); i++)
            cerr << gracz1[i] << endl;
        cerr << endl;    
        for(int i=0; i<gracz2.size(); i++)
            cerr << gracz2[i] << endl;
       
        
    }
    // Write an action using cout. DON'T FORGET THE "<< endl"
    // To debug: cerr << "Debug messages..." << endl;
    if(remis)
         cout << "PAT" << endl;
    else if (gracz1.empty())
         cout << 2 << " " << nrGames <<endl;
         else
         cout << 1 << " " << nrGames <<endl;
}