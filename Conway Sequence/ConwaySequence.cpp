#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
 string ConSeq(int L, int R);
int main()
{
    int R;
    cin >> R; cin.ignore();
    int L;
    cin >> L; cin.ignore();

    // Write an action using cout. DON'T FORGET THE "<< endl"
    // To debug: cerr << "Debug messages..." << endl;

    cout << ConSeq(L,R) << endl;
    //cout << "odpowiedz" << endl;
}

string ConSeq(int L, int R) 
{
    if(L==1) {
        return to_string(R);
    }
    vector<int> prevLevel;
    vector<int> currLevel;
    
    prevLevel.push_back(R);
    int cnt;
    int currNumber;
    for(int k = 1; k < L; k++)
    {
        currLevel.clear();
        currNumber = prevLevel[0];
        cnt=1;
        if(prevLevel.size()==1) 
            {
                currLevel.push_back(cnt);
                currLevel.push_back(currNumber);
            }
        for(int i = 1; i < prevLevel.size(); i++)
        {
            
            if(prevLevel[i] != currNumber && i != prevLevel.size()-1)
            {
                currLevel.push_back(cnt);
                currLevel.push_back(currNumber);
                currNumber= prevLevel[i];
                cnt=1;
            }
            else if ( prevLevel[i] != currNumber && i == prevLevel.size()-1 )
            {
                currLevel.push_back(cnt);
                currLevel.push_back(currNumber);
                currLevel.push_back(1);
                currLevel.push_back(prevLevel[i]);
            }
            else
            {
                cnt++;
                if ( i == prevLevel.size()-1)
                {
                    currLevel.push_back(cnt);
                    currLevel.push_back(currNumber);
                }
            }
        }
        prevLevel.clear();
        for(int a : currLevel)
            prevLevel.push_back(a);
    }
    string result ="";
    for (int a : currLevel)
        result += to_string(a)+" ";
    result.pop_back();
    return result;
}