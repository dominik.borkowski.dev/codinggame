#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
 
int main()
{
    int N;
    unsigned long long L = 0;
    unsigned long long deltaX = 0;
    unsigned long long sumY= 0;
    long double mediana= 0;
    int minX;
    int maxX;
    vector<int> igreks; 
    cin >> N; cin.ignore();
    for (int i = 0; i < N; i++) {
        int X;
        int Y;
        cin >> X >> Y; cin.ignore();
        sumY += Y;
        igreks.push_back(Y);
        if ( i == 0 ) 
        {
            maxX = X;
            minX = X;
        }
        if( X > maxX) maxX = X;
        if( X < minX) minX = X;
    }
    deltaX = maxX - minX;
    std::sort(igreks.begin(), igreks.end());    
    if(N%2 == 0) {
        mediana = (igreks[(N/2)-1] + igreks[N/2]) / 2;
    }
    else {
        mediana = igreks[(N-1)/2];
    }
    // for (auto a : igreks) 
    //     cerr << a << " " << endl;
    // Write an action using cout. DON'T FORGET THE "<< endl"
    // To debug: cerr << "Debug messages..." << endl;
    L=deltaX;
    for(auto i: igreks)
    {
       if(mediana >= 0 && i>=0 || mediana <= 0 && i<=0)            
                L += abs( abs(mediana)-abs(i) ); 
        else                       
                L += abs( abs(mediana)+abs(i) ); 
    }
    // cerr << "L: " << L << endl;
    // cerr << "N " << N << endl;
    // cerr << endl;
    cout << L << endl;

    //cout << "answer" << endl;
}