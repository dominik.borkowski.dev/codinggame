//https://www.codingame.com/ide/puzzle/aneo
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <list>
#include <utility>

using namespace std;
/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
 double KMHtoMS(int speed)
 {
     return (1000.00*speed)/3600.00;
 }

//Numerical method:
class TtrafLight
{
    private:
        int distance;
        int duration;
    public:
        bool isSpeedOk(int speed); 
        TtrafLight(int dis, int dur):distance(dis),duration(dur){}
        friend ostream& operator<<(ostream& s, const TtrafLight& tl);
};
ostream& operator<<(ostream& s, const TtrafLight& tl)
{
    s << "dist:" << tl.distance << " dur:" << tl.duration;
    return s;
}
bool TtrafLight::isSpeedOk(int speed)
{
    float arrTime = distance/KMHtoMS(speed);//arive time
    int tDiv = arrTime/duration;
    return (tDiv%2==0) ? true : false;
}

int main()
{
    //list of intervals, on which calculations will be performed
    int speed;
    cin >> speed; cin.ignore();
    int lightCount;
    cin >> lightCount; cin.ignore();
    list<TtrafLight> lights;
    for (int i = 0; i < lightCount; i++) {
        int distance;
        int duration;
        cin >> distance >> duration; cin.ignore();
        TtrafLight lt(distance,duration);
        //cerr << lt << endl;
        lights.push_back(lt);
    }
    list<TtrafLight>::iterator it=lights.begin();
    int tmpSpeed = speed;
    while(it!=lights.end())
    {
        if (it->isSpeedOk(tmpSpeed))
        {   
            ++it;
        }
        else
        {
            it = lights.begin();
            --tmpSpeed;
        }
    }
    // Write an action using cout. DON'T FORGET THE "<< endl"
    // To debug: cerr << "Debug messages..." << endl;

    cout << tmpSpeed << endl;
}