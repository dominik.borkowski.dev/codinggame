import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int L = in.nextInt();
        int C = in.nextInt();
        Bender bender = new Bender(L,C);
        if (in.hasNextLine()) {
            in.nextLine();
        }
        for (int i = 0; i < L; i++) {
            String row = in.nextLine();
            bender.addRow(row);
            System.err.println(row);
        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");
        bender.solve();
        System.err.println();
        System.out.println(bender);
    }
}

class Bender {
    private int L; 
    private int C;
    private int X;
    private int Y;
    private char currDirection;
    private boolean inverted;
    private boolean breaker;
    private boolean looped;
    //private char[] priorityOrder = {'S', 'E', 'N', 'W'};
    List<Cell>trace = new ArrayList<>();
    List<String> map = new ArrayList<>();
    
    public void solve(){
        setStartPoint();
        System.err.println("Start Point: "+X+","+Y);
        while(map.get(X).charAt(Y)!='$' && !looped){
            move();
            char cellContent =  map.get(X).charAt(Y);
            switch (cellContent) {
                case ' ':
                    break;
                case 'N':
                    currDirection = 'N';
                    break;
                case 'S':
                    currDirection = 'S';
                    break;
                case 'W':
                    currDirection = 'W';
                    break;
                case 'E':
                    currDirection = 'E';
                    break;
                case 'B':
                    breaker = (breaker) ? false : true;
                    //System.err.println("breaker");
                    break;
                case 'I':
                    inverted = (inverted) ? false : true;
                    break;
                case 'T':
                    //System.err.println("Before teleport: " + X+","+Y);
                    teleport();
                    //System.err.println("After teleport: " + X+","+Y);
                    break;
            }
        }
       // System.err.println("trace:");for (Cell c : trace)  System.err.print(c.arrDir);
    }
    private void teleport() {
        for(int i=0; i < map.size(); i++)
            for(int j=0; j<map.get(i).length(); j++) {
                if(map.get(i).charAt(j) == 'T' && ( X!= i || Y!=j) ){
                        X = i;
                        Y = j;
                        return;                    
                }
            }                
    }
    private void move(){
        int newX=X;
        int newY=Y;
        switch(currDirection) {
         case 'N':
             newX--;
             break;
         case 'S':
             newX++;
             break;
         case 'E':
             newY++;
             break;             
         case 'W':
             newY--;
             break;
         default:
            System.err.println("Something is no yes");
        }
        
        char newCellChar = map.get(newX).charAt(newY);
        if (newCellChar == '#' || (newCellChar == 'X' && !breaker)) {
            newX=X;
            newY=Y;
        //    System.err.println("ommit: " + ommitObstacle() );
           currDirection = ommitObstacle();
           switch(currDirection) {
                 case 'N':
                     newX = X - 1;
                     System.err.println("N: "+map.get(X).charAt(Y));                   
                     break;
                 case 'S':
                     newX = X + 1;
                     System.err.println("S: "+map.get(X).charAt(Y));
                     break;
                 case 'E':
                     newY = Y + 1;
                     System.err.println("E: "+map.get(X).charAt(Y));
                     break;             
                 case 'W':
                     newY = Y - 1;
                     System.err.println("W: "+map.get(X).charAt(Y));
                     break;
                 default:
                    System.err.print("Something is no yes");
            }
             X=newX;
             Y=newY;
        }
        else {
             X=newX;
             Y=newY;
             if (newCellChar == 'X') {
                String tmpRow = map.get(X);
                tmpRow = tmpRow.substring(0,Y)+'&'+tmpRow.substring(Y+1);
                map.remove(X);
                map.add(X,tmpRow);
                //for(String s : map) System.err.println(s);
              }
             System.err.println(currDirection+":: "+map.get(X).charAt(Y));
        }
        Cell tmpCell = new Cell(X,Y,currDirection,inverted,breaker);
        for(Cell c : trace)
          if(c.equals(tmpCell)) {
              System.err.println("oh no, loop");
             looped = true;
             return;
          }
        trace.add(tmpCell);
    }
    private char ommitObstacle() {
        if(!inverted) {
            if( (map.get(X+1).charAt(Y) != '#' && map.get(X+1).charAt(Y) != 'X' ) || (map.get(X+1).charAt(Y)=='X' && breaker) )
                return 'S';
            if( (map.get(X).charAt(Y+1) != '#' && map.get(X).charAt(Y+1) != 'X' ) || (map.get(X).charAt(Y+1)=='X' && breaker) )
                return 'E'; 
            if( (map.get(X-1).charAt(Y) != '#' && map.get(X-1).charAt(Y) != 'X' ) || (map.get(X-1).charAt(Y)=='X' && breaker) )
                return 'N';
            if( (map.get(X).charAt(Y-1) != '#' && map.get(X).charAt(Y-1) != 'X' ) || (map.get(X).charAt(Y-1)=='X' && breaker) )
                return 'W';
        }
        else{
            //System.err.println("ommitinverted: " + map.get(X).charAt(Y) );
            if( (map.get(X).charAt(Y-1) != '#' && map.get(X).charAt(Y-1) != 'X' ) || (map.get(X).charAt(Y-1)=='X' && breaker) )
                return 'W';
            if( (map.get(X-1).charAt(Y) != '#' && map.get(X-1).charAt(Y) != 'X' ) || (map.get(X-1).charAt(Y)=='X' && breaker) )
                return 'N';
            if( (map.get(X).charAt(Y+1) != '#' && map.get(X).charAt(Y+1) != 'X' ) || (map.get(X).charAt(Y+1)=='X' && breaker) )
                return 'E'; 
            if( (map.get(X+1).charAt(Y) != '#' && map.get(X+1).charAt(Y) != 'X' ) || (map.get(X+1).charAt(Y)=='X' && breaker) )
                return 'S';
        }
        return 'd';
    }
    public void addRow(String row) {map.add(row);}
    private void setStartPoint() {
        for (int i=0; i<L; i++) {
            for(int j=0; j<C; j++) {
                if(map.get(i).charAt(j)=='@') {
                    X=i;
                    Y=j;
                }
            }
        }
    }
    public Bender(int L, int C) {
        this.L = L;
        this.C = C;
        inverted = false;
        breaker = false;
        looped = false;
        currDirection = 'S';
    }
    class Cell {
        public int X;
        public int Y;
        public char arrDir; //arrival Direction
        public boolean cinverted;
        public boolean cbreaker;
        List<String> cmap;
        //current map state? 
        @Override
        public boolean equals(Object o) {
            Cell c = (Cell)o;
            if(X==c.X && Y==c.Y && arrDir==c.arrDir &&
                cinverted==c.cinverted && cbreaker == c.cbreaker) {
                    
                for(int i=0; i < map.size(); i++)
                    if (map.get(i) != cmap.get(i))
                        return false;
                    
                return true;
            }
            return false;
        }
        public Cell(int X, int Y, char arrDir, boolean cinverted, boolean cbreaker) {
            this.X = X;
            this.Y = Y;
            this.arrDir = arrDir;
            this.cinverted=cinverted;
            this.cbreaker=cbreaker;
            cmap = new ArrayList<>();
            for(String s: map)
                cmap.add(s);
        }
    }
    @Override
    public String toString(){
        if(looped)
            return "LOOP";
        String result = "";
        for(Cell c : trace)
            if (c.arrDir == 'N') result +="NORTH\n";
            else if (c.arrDir == 'S') result +="SOUTH\n";
            else if (c.arrDir == 'E') result +="EAST\n";
            else if (c.arrDir == 'W') result +="WEST\n";
        return result;
    }
}