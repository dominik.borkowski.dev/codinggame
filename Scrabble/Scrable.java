import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    public static void main(String args[]) {
        Slownik slownik = new Slownik();
        
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        System.err.println("N: " + N);
        if (in.hasNextLine()) {
            in.nextLine();
        }
        for (int i = 0; i < N; i++) {
            String W = in.nextLine();
            if (W.length() <=7)
                slownik.addWord(W);
        }
        String LETTERS = in.nextLine();
        slownik.setAvailableLetters(LETTERS);    
        //System.err.println("slownik size : " + slownik.size());
        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");
        //System.err.println("wynik: "+ slownik.getBestWord());
        System.out.println(slownik.getBestWord());
    }
}
class Slownik {
    private Vector vSlow;
    public void addWord(String slowo) {
        vSlow.add(slowo);
        
    }
    private String availableLetters;
    private int maxPointsFromLetters;
    private int calculatePoints(String s){
        int wynik =0;
        for(int i=0; i< s.length(); i++)
            wynik += lPoints.get(s.charAt(i));
        return wynik;
    }
    public void setAvailableLetters(String letters) { 
        this.availableLetters = sort(letters);
        maxPointsFromLetters = calculatePoints(this.availableLetters);        
        System.err.println("Letters: " + this.availableLetters + " maxPoints " + maxPointsFromLetters);    
    }
    private String bestWord;
    public static String sort(String s){
        char[] chars = s.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }
    private boolean canBeBuild(String s) {
        s=sort(s);
        String tmp = availableLetters;
        //String tmp2 = "";
        for(int i=0; i<s.length(); i++) {
            int indeks = tmp.indexOf(s.charAt(i));
            if(indeks>0) {
                tmp = tmp.substring(0,indeks-1) + tmp.substring(indeks+1);
            }
            else if (indeks ==0) {
                tmp = tmp.substring(1);
            }
            else {
                return false;
            }
        }
        
        return true;
    }
    public String getBestWord() {
        int bestScore =0;
        for(Object o : vSlow) {
            String slowo = (String)o;            
            int tmpPoints = calculatePoints(slowo);
            if(slowo.equals("eurasia") || slowo.equals("satire")||slowo.equals("upstate"))
            System.err.println(slowo +" "+tmpPoints);
            if(slowo.equals("upstate"))
                System.err.println("upstatde can be? " + canBeBuild("upstate"));
            if(canBeBuild(slowo) && tmpPoints<=maxPointsFromLetters) {
                if(tmpPoints > bestScore){
                    bestScore = tmpPoints;
                    bestWord = slowo;
                }
            }
        }        
        return bestWord;
    }
    public Hashtable<Character, Integer> lPoints;
    public Slownik() {
        vSlow = new Vector();
        lPoints = new Hashtable<Character, Integer>();
        initializeHashTable();
    }
    private void initializeHashTable() {
        char[] onePointLetters = {'e','a','i','o','n','r','t','l','s','u'};
        char[] twoPointLetters = {'d','g'};
        char[] threePointLetters = {'b','c','m','p'};
        char[] fourPointLetters = {'f','h','v','w','y'};
        char[] fivePointLetters = {'k'};
        char[] eightPointLetters= {'j','x'};
        char[] tenPointLetters  = {'q','z'};
        for (int i=0; i<onePointLetters.length;   i++) lPoints.put(onePointLetters[i], 1);        
        for (int i=0; i<twoPointLetters.length;   i++) lPoints.put(twoPointLetters[i], 2);
        for (int i=0; i<threePointLetters.length; i++) lPoints.put(threePointLetters[i], 3);
        for (int i=0; i<fourPointLetters.length;  i++) lPoints.put(fourPointLetters[i], 4);
        for (int i=0; i<fivePointLetters.length;  i++) lPoints.put(fivePointLetters[i], 5);
        for (int i=0; i<eightPointLetters.length; i++) lPoints.put(eightPointLetters[i], 8);
        for (int i=0; i<tenPointLetters.length;   i++) lPoints.put(tenPointLetters[i], 10);
    }
}