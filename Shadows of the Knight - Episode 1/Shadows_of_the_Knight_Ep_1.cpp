#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
    int W; // width of the building.
    int H; // height of the building.
    cin >> W >> H; cin.ignore();
   // cerr << "W:" << W << "\t H:" << H << endl;
    int N; // maximum number of turns before game over.
    cin >> N; cin.ignore();
    int X0;
    int Y0;
    int cX;//obecne polozenie batmana
    int cY;
    int lowest, upper, lefter, righter; //najbardziej skrajne polozenia w jakich byl
    int X, Y; 
    cin >> X0 >> Y0; cin.ignore();
    cX=X0;
    cY=Y0;
    X=cX;
    Y=cY;
    upper  = -1;
    lowest = H+1;
    lefter = -1;
    righter= W+1;
    int kier=0; 
    // game loop
    while (1) {
        string BOMB_DIR; // the direction of the bombs from batman's current location (U, UR, R, DR, D, DL, L or UL)
        cin >> BOMB_DIR; cin.ignore();
        // Write an action using cout. DON'T FORGET THE "<< endl"
        // To debug: cerr << "Debug messages..." << endl;
       // BOMB_DIR="L";
        if(BOMB_DIR=="U") kier = 1;
        else if (BOMB_DIR=="UR") kier = 2;
        else if (BOMB_DIR=="R") kier = 3;
        else if (BOMB_DIR=="DR") kier = 4;
        else if (BOMB_DIR=="D") kier = 5;
        else if (BOMB_DIR=="DL") kier = 6;
        else if (BOMB_DIR=="L") kier = 7;
        else if (BOMB_DIR=="UL") kier = 8;
        cerr <<"kier:"<< kier <<endl; 
        switch (kier) 		//U, UR, R, DR, D, DL, L or UL
        {
            case 1:
                Y=(cY+upper)/2;
                lowest=cY;
                break;
            case  2:
                Y=(cY+upper)/2;
                X=(righter+cX)/2;
                lowest=cY;
                lefter=cX;
                break;
            case  3:
                X=(righter+cX)/2;
                lefter=cX;
                break;
            case 4:
                Y=(lowest+cY)/2;
                X=(righter+cX)/2;
                upper=cY;
                lefter=cX;
                break;
            case 5:
                Y=(lowest+cY)/2;
                upper=cY;
                break;
            case 6:
                Y=(lowest+cY)/2;
                X=(cX+lefter)/2;
                upper=cY;
                righter=cX;
                break;
            case 7:
                X=(cX+lefter)/2;
                righter=cX;
                break;
            case 8:
                Y=(cY+upper)/2;
                X=(cX+lefter)/2;
                lowest=cY;
                righter=cX;
                break;
            default:
                cerr << "something went wrong" << endl;
                break;
        }
        cout << X << " " << Y << endl;
        cX=X;
        cY=Y;
        //cout << "1 0" << endl; // the location of the next window Batman should jump to.
    }
}