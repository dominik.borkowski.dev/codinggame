#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
    int n;
    vector <int> values;
    int max{0};
    int min{0};
    int delta;
    int maxDelta{0};
    cin >> n; cin.ignore();
    for (int i = 0; i < n; i++) {
        int v;
        cin >> v; cin.ignore();
        if (i == 0)
        {
            max = v;
            min = v;
        }
        values.push_back(v);
    }
    for(const auto & v : values)
    {
        if(v > max) 
        {
            delta = min-max;
            max = v;
            min = v;
        }
        else if( v < min)
        {
            min = v;
            delta = min - max;
        }
        if(maxDelta> delta)
            maxDelta = delta;
    }
    
    // Write an action using cout. DON'T FORGET THE "<< endl"
    // To debug: cerr << "Debug messages..." << endl;

    cout << maxDelta << endl;
}